# UNIX-Einführung



## Einleitung

Hier sind nochmal alle in der Vorlesung besprochenen UNIX-Kommandos aufgelistet. Über den Kommandos ist nochmal als Kommentar die Wirkung angegeben.

## Navigation

```console

# Anzeigen des aktuellen Arbeitsverzeichnisses
pwd

# Wechsel in ein Verzeichnis
cd <Verzeichnisname> 

# Wechsel in das übergeordnete Verzeichnis
cd .. 

# Wechsel in das eigene Nutzerverzeichnis
cd ~ 

# Wechsel in ein untergeordnetes  
cd <Verzeichnis1>/<Verzeichnis2>

```


##  Verzeichnisse

```console

# Auflisten des Verzeichnisinhalts
ls

# Auflisten des Verzeichnisinhalts als Liste
ls -l

# Auflisten des Verzeichnisinhalts als Liste mit "versteckten" Dateien
ls -la

# Auflisten des Verzeichnisinhalts eines untergeordneten Verzeichnisses
ls <Verzeichnisname> 

# Verzeichnis erstellen
mkdir <verzeichnisname>

# Verzeichnis löschen

rm -rf <verzeichnisname>

```

## Dateien

```console
# Leere Datei erstellen
touch <dateiname>

# Löschen einer Datei
rm <dateiname>

# Bearbeiten einer Datei mit dem Editor vim
vim <dateiname>
```

## Compiler-Kommandos

```console
# Kompilieren einer .c Datei
gcc <dateiname.c>

# Ausführen einer kompilierten Datei
a.out

# Kompilieren einer .c Datei mit Namensvergabe für die ausführbare Datei
gcc -o <nameDerAusführbarenDatei> <NameDesSourceFiles>.c

```

# VIM

Der Editor VIM enthält viele Kommandos und ist ein quasi "allround" Werkzeug für die Entwicklung 

Zum starten des Editors reicht das Kommando `vim`. Jedoch solle am besten direkt ein Dateiname mitgegeben werden z.B. `vim hello.c`. Das erleichtert das Speichern. Schon vorhandene Dateien können auch mit dem Kommando `vim <Dateiname>` geöffnet werden.

## Modi

In VIM gibt es mehrere Modi hier die wichtigsten:

| Taste  | Modus  | Bedeutung  |
|---|---|---|
|  ESC | Kommandomodus  |  In diesem Modus befindet sich VIM beim Start. Hier kann z.B. in den Insert Modus gewechselt oder die Datei abgespeichert werden  |
|  i | Insert Modus  | Dieser Modus ermöglicht das Schreiben in der geöffneten Datei  |


**Sie können immer durch das Drücken der ESC-Taste in den Kommandomodus zurück.**

Vim Hat viele weitere Modi eine Referenz finden sie [hier](https://vim.rtorr.com/).

## Kommandos

Durch das Erlernen der VIM-Kommandos ersparen Sie sich viel Zeit. In der folgenden Tabelle finden Sie die wichtigsten:

Alle Kommandos die mit einem `:`eingeleitet werden, können Sie im unteren Bereich des Editors sehen und beliebig kombinieren.


| Taste(n)  | Wirkung  | Modus in dem man sich befinden muss  |
|---|---|---|
| dd  | Löscht die Zeile über dem der Cursor steht  | Kommando  |
|  :w | Speichern der Datei |  Kommando |
| :q  |  Verlassen des Editors |  Kommando |
| :wq  |   Datei speichern und Editor verlassen |  Kommando |
| :q!  |   Erzwungenes Verlassen |  Kommando |
|  :w! | Erzwungenes Speichern der Datei |  Kommando |
|  ZZ |  Datei speichern und Editor verlassen |  Kommando |

Weitere Kommandos finden Sie [hier](https://vim.rtorr.com/).